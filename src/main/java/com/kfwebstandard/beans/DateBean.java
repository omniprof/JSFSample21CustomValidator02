package com.kfwebstandard.beans;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 * Backing beans are just managed beans that are used directly by the user
 * interface in forms. Validation code can be part of the bean and called upon
 * by the validation attribute of the appropriate tag
 *
 */
@Named("date")
@RequestScoped
public class DateBean implements Serializable {

    private int day;
    private int month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int newValue) {
        day = newValue;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int newValue) {
        month = newValue;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int newValue) {
        year = newValue;
    }

    /**
     * The method that performs validation accepts these three parameters
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateDate(FacesContext context, UIComponent component,
            Object value) {

        // These values have not yet been added to the bean
        // so they must be read from the user interface
        UIInput dayInput = (UIInput) component.findComponent("day");
        UIInput monthInput = (UIInput) component.findComponent("month");

        int d = ((Integer) dayInput.getLocalValue());
        int m = ((Integer) monthInput.getLocalValue());
        int y = ((Integer) value);

        if (!isValidDate(d, m, y)) {
            FacesMessage message = com.kfwebstandard.util.Messages.getMessage(
                    "com.kfwebstandard.bundles.messages", "invalidDate", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    private static boolean isValidDate(int d, int m, int y) {
        if (d < 1 || m < 1 || m > 12) {
            return false;
        }
        if (m == 2) {
            if (isLeapYear(y)) {
                return d <= 29;
            } else {
                return d <= 28;
            }
        } else if (m == 4 || m == 6 || m == 9 || m == 11) {
            return d <= 30;
        } else {
            return d <= 31;
        }
    }

    private static boolean isLeapYear(int y) {
        return y % 4 == 0 && (y % 400 == 0 || y % 100 != 0);
    }
}
